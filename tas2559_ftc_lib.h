/*
** =============================================================================
** Copyright (c) 2016  Texas Instruments Inc.
**
** File:
**     tas2559_lib_ftc.h
**
** Description:
**     header file for tas2559_lib_ftc.c
**
** =============================================================================
*/

#ifndef TAS2559_LIB_FTC_H_
#define TAS2559_LIB_FTC_H_

#include <stdint.h>
#include <stdbool.h>

#include "tas2559.h"

enum FTCDev {
	DevNone,
	DevA = 0x01,
	DevB = 0x02,
	DevBoth = (DevA | DevB)
};

#define	FTCLIB_TAS2557	0x00000000
#define	FTCLIB_TAS2559	0x01000000

int get_lib_ver(void);
int tas2559_ftc_start(enum FTCDev, double re_ppc3);
int set_re(enum FTCDev, double re_ppc3, double re, double alpha);
int set_temp_cal(enum FTCDev, uint32_t prm_pow, uint32_t prm_tlimit);
double get_re(enum FTCDev, double re_ppc3);
double CalcRe(double re_ppc3, uint32_t prm_r0);
uint32_t calc_prm_pow(double re, double delta_t_max, double nRTV, double nRTM, double nRTVA, double nSysGain);
uint32_t calc_prm_tlimit(double delta_t_max, double alpha, double nDevNonlinPer, double nRTV, double nRTM, double nRTVA, double nPIG);
void tas2559_ftc_stop(void);

/* below functions are used in SPK measurement only */
int get_Re_deltaT(enum FTCDev, double nPPC3_Re, double nPPC3_alpha, double *pnRe, double *pnDeltaT);
int get_f0_Q(enum FTCDev, double nPPC3_FWarp, double nPPC3_nFS, double nPPC3_Bl, double nPPC3_Mms, double nPPC3_Re0, double *pnF0, double *pnQ);

#endif /* TAS2559_LIB_FTC_H_ */
