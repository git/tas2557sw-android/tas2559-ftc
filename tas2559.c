/*
** =============================================================================
** Copyright (c) 2016  Texas Instruments Inc.
**
** File:
**     tas2559.c
**
** Description:
**     functions for configurating TAS2559 Android device
**
** =============================================================================
*/
#include <sys/types.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <string.h>

#include "tas2559.h"
#include "system.h"

#define TILOAD_NODE "/dev/tiload_node"

static int gTILoad = 0;
static int gBinFile = 0;

#define MAX_BIN_SIZE 2048
static char gpBin[MAX_BIN_SIZE];
static unsigned int gnBinIndex = 0;
static unsigned int gnBinBlockIndex = 0;
static char gpDevABlock[MAX_BIN_SIZE];
static unsigned int gnDevABlockIndex = 0;
static unsigned char gnDev;

static void tas2559_check_node(void);

void tas2559_mixer_command(char *pCommand, int nData)
{
	char *pMixer[] = {AUDIO_MIXER, pCommand, "0", NULL};
	char *pEnv[] = {NULL};

	char pData[16];

	printf("TAS2559 mixer command %s = %d.\n\r", pCommand, nData);

	sprintf(pData, "%d", nData);
	pMixer[2] = pData;
	
	if (0 == (fork()))
	{
		if (execve(AUDIO_MIXER, pMixer, pEnv) == -1)
		{
			printf("factorytest: Can't find mixer. Please install %s. \n\r", AUDIO_MIXER);
			exit(-1);
		}
	}

	sys_delay(500);
}

void tas2559_load_calibration(int nCalibration)
{
	tas2559_check_node();

	ioctl(gTILoad, TILOAD_IOCTL_SET_CALIBRATION, &nCalibration);
	sys_delay(500);
}

static void tas2559_check_node(void)
{
	if (gTILoad)
		return;

	gTILoad = open(TILOAD_NODE, O_RDWR);

	if (gTILoad < 0) {
		printf("factorytest: Can't find tiload. Please create %s. \n\r", TILOAD_NODE);
		exit(-1);
	}

	printf("factorytest: %s handle = %d\n\r", TILOAD_NODE, gTILoad);
}

uint32_t tas2559_coeff_read(uint32_t reg)
{
	uint8_t nbook = TAS2559_BOOK_ID(reg);
	uint8_t npage = TAS2559_PAGE_ID(reg);
	uint8_t nreg = TAS2559_PAGE_REG(reg);
	unsigned char pPageZero[] = {0x00, 0x00};
	unsigned char pBook[] = {0x7F, nbook};
	unsigned char pPage[] = {0x00, npage};
	unsigned char pData[] = {nreg, 0x00, 0x00, 0x00, 0x00};

	tas2559_check_node();

	write(gTILoad, pPageZero, 2);
	write(gTILoad, pBook, 2);
	write(gTILoad, pPage, 2);
	read(gTILoad, pData, 4);

	return ((pData[0] << 24) | (pData[1] << 16) | (pData[2] << 8) | (pData[3]));
}

uint32_t tas2559_switch_device(uint8_t i2cslave)
{
	int addr = i2cslave;

	tas2559_check_node();

	gnDev = i2cslave;

	ioctl(gTILoad, TILOAD_IOCTL_SET_CHL, &addr);

	return 0;
}

#define TAS2559_mCDSP_SWAP_REG	TAS2559_REG(0x8c, 0x2a, 0x54)

void tas2559_coeff_write(uint32_t reg, uint32_t data)
{
	unsigned int nByte;
	uint8_t nbook, npage, nreg;

	nbook = TAS2559_BOOK_ID(reg);
	npage = TAS2559_PAGE_ID(reg);
	nreg = TAS2559_PAGE_REG(reg);

	// if the bin file is open, write the coefficients to the bin file
	if (gBinFile) {
		unsigned int index = gnDevABlockIndex*4;

		gpDevABlock[index] = 0;
		gpDevABlock[index + 1] = 4;
		gpDevABlock[index + 2] = 0x85;
		gpDevABlock[index + 4] = nbook;
		gpDevABlock[index + 5] = npage;
		gpDevABlock[index + 6] = nreg;

		for (nByte = 0; nByte < 4; nByte++)
			gpDevABlock[index + 7 + nByte] = (data >> ((3 - nByte) * 8)) & 0xFF;

		gnDevABlockIndex += 3;
	} else {
		unsigned char pPageZero[] = {0x00, 0x00};
		unsigned char pBook[] = {0x7F, nbook};
		unsigned char pPage[] = {0x00, npage};
		unsigned char pData[] = {nreg, (data & 0xFF000000) >> 24, (data & 0x00FF0000) >> 16, (data & 0x0000FF00) >> 8, data & 0x000000FF};

		tas2559_check_node();

		write(gTILoad, pPageZero, 2);
		write(gTILoad, pBook, 2);
		write(gTILoad, pPage, 2);
		write(gTILoad, pData, 5);

		pBook[1] = TAS2559_BOOK_ID(TAS2559_mCDSP_SWAP_REG);
		pPage[1] = TAS2559_PAGE_ID(TAS2559_mCDSP_SWAP_REG);

		pData[0] = TAS2559_PAGE_REG(TAS2559_mCDSP_SWAP_REG);
		pData[1] = 0x00;
		pData[2] = 0x00;
		pData[3] = 0x00;
		pData[4] = 0x01;

		write(gTILoad, pPageZero, 2);
		write(gTILoad, pBook, 2);
		write(gTILoad, pPage, 2);
		write(gTILoad, pData, 5);
	}
}

void tas2559_save_cal(struct TFTCConfiguration *pFTCC, 
	double dev_a_re, uint32_t dev_a_rms_pow, uint32_t dev_a_t_limit, 
	double dev_b_re, uint32_t dev_b_rms_pow, uint32_t dev_b_t_limit, 
	double t_cal,  uint32_t nResult, char * pFileName)
{
	printf("TAS2559 calibration values:\n\r");
	printf("    DevA Re = %1.2f Ohm\n\r", dev_a_re);
	printf("    DevA rms_pow       = 0x%08X\n\r", dev_a_rms_pow);
	printf("    DevA t_limit       = 0x%08X\n\r", dev_a_t_limit);
	printf("    DevB Re = %1.2f Ohm\n\r", dev_b_re);
	printf("    DevB rms_pow       = 0x%08X\n\r", dev_b_rms_pow);
	printf("    DevB t_limit       = 0x%08X\n\r", dev_b_t_limit);

	if((nResult & RE1_CHK_MSK) == RESULT_PASS)
		printf(" SPK A calibration success! \n\r");
	else {
		if(nResult & RE1_FAIL_HI)
			printf(" SPK A Calibration fail : Re is too high (limit: %1.2f).\n\r", pFTCC->nTSpkCharDevA.nReHi);
		else
			printf(" SPK A Calibration fail : Re is too low (limit: %1.2f).\n\r", pFTCC->nTSpkCharDevA.nReLo);
	}
	
	if((nResult & RE2_CHK_MSK) == RESULT_PASS){
		printf(" SPK B calibration success! \n\r");
	}else {
		if(nResult & RE2_FAIL_HI)
			printf(" SPK B Calibration fail : Re is too high (limit: %1.2f).\n\r", pFTCC->nTSpkCharDevB.nReHi);
		else
			printf(" SPK B Calibration fail : Re is too low (limit: %1.2f).\n\r", pFTCC->nTSpkCharDevB.nReLo);
	}
	
	FILE *pFile = fopen(pFileName, "w+");
	
	fprintf(pFile, "DevA Re = %1.2f\n\r", dev_a_re);
	fprintf(pFile, "DevA rms_pow       = 0x%08X\n\r", dev_a_rms_pow);
	fprintf(pFile, "DevA t_limit       = 0x%08X\n\r", dev_a_t_limit);
	fprintf(pFile, "DevB Re = %1.2f\n\r", dev_b_re);
	fprintf(pFile, "DevB rms_pow       = 0x%08X\n\r", dev_b_rms_pow);
	fprintf(pFile, "DevB t_limit       = 0x%08X\n\r", dev_b_t_limit);	
	fprintf(pFile, "Ambient temperature = %2.2f\n\r\n\r", t_cal);
	fprintf(pFile, "Result = 0x%x\n\r\n\r", nResult);

	fclose(pFile);
}

#define DDC_DESCRIPTION "Calibration Data File for TAS2559"
#define CALIBRATION_DESCRIPTION "Calibration snapshot for TAS2559"
#define DATA_DESCRIPTION "data blocks for TAS2559 FTC"

void tas2559_open_bin(char * pFileName)
{
	time_t timestamp; 

	gBinFile = open(pFileName, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR | S_IROTH | S_IWOTH);

	gnBinIndex = 0;
	memset(gpBin, 0, MAX_BIN_SIZE);
	memset(gpDevABlock, 0, MAX_BIN_SIZE/2);
	gnDevABlockIndex = 0;

	//magic number
	gpBin[gnBinIndex++] = '5';
	gpBin[gnBinIndex++] = '5';
	gpBin[gnBinIndex++] = '5';
	gpBin[gnBinIndex++] = '2';

	gnBinIndex += 4; 	/* bypass size int */
	gnBinIndex += 4; 	/* bypass checksum int */
	gnBinIndex += 4; 	/* bypass PPC3 version int */
	gnBinIndex += 4; 	/* bypass DSP version int */

	gpBin[gnBinIndex++] = 0;
	gpBin[gnBinIndex++] = 0;
	gpBin[gnBinIndex++] = 1;
	gpBin[gnBinIndex++] = 0; /* driver version 0x00000100 */

	time(&timestamp);
	gpBin[gnBinIndex++] = (unsigned char)((timestamp&0xff000000)>>24);
	gpBin[gnBinIndex++] = (unsigned char)((timestamp&0x00ff0000)>>16);;
	gpBin[gnBinIndex++] = (unsigned char)((timestamp&0x0000ff00)>>8);;
	gpBin[gnBinIndex++] = (unsigned char)((timestamp&0x000000ff));;

	strcpy(&gpBin[gnBinIndex], "Calibration Data File");	/* DDC name */
	gnBinIndex += 64;

	strcpy(&gpBin[gnBinIndex], DDC_DESCRIPTION);
	gnBinIndex += strlen(DDC_DESCRIPTION) + 1;

	gnBinIndex += 4; /* device family index */

	gpBin[gnBinIndex++] = 0; /* device index */
	gpBin[gnBinIndex++] = 0; /* device index */
	gpBin[gnBinIndex++] = 0; /* device index */
	gpBin[gnBinIndex++] = 4; //device index, TAS2559

	gnBinIndex +=
		2 +	/* num PLL index */
		0 +	/* array PLL index */
		2 +	/* num programs index */
		0 +	/* array programs index */
		2 +	/* num configurations index */
		0;	/* array configurations index */

	gpBin[gnBinIndex++] = 0x00;
	gpBin[gnBinIndex++] = 0x01; /* one calibration data block */

	strcpy(&gpBin[gnBinIndex], "Calibration snapshot");
	gnBinIndex += 64;
	strcpy(&gpBin[gnBinIndex], CALIBRATION_DESCRIPTION);
	gnBinIndex += strlen(CALIBRATION_DESCRIPTION) + 1;

	gpBin[gnBinIndex++] = 0x00; /* compatible program = smart amp (index 0) */
	gpBin[gnBinIndex++] = 0x00; /* compatible configuration */

	strcpy(&gpBin[gnBinIndex], "Calibration Data");
	gnBinIndex += 64;
	strcpy(&gpBin[gnBinIndex], DATA_DESCRIPTION);
	gnBinIndex += strlen(DATA_DESCRIPTION) + 1;

	gpBin[gnBinIndex++] = 0x00; 
	gpBin[gnBinIndex++] = 1; 	// one blocks

	gpDevABlock[0] = 0;
	gpDevABlock[1] = 0;
	gpDevABlock[2] = 0;
	gpDevABlock[3] = 0x03;	/* COEFF_DEVICE_A – 0x03 */
	gnDevABlockIndex = 2;

}

void tas2559_close_bin(void)
{
	unsigned int nCommands;
	unsigned char pCommit[] = {
		0x00, 0x04, 0x85, 0x00,
		TAS2559_BOOK_ID(TAS2559_mCDSP_SWAP_REG), TAS2559_PAGE_ID(TAS2559_mCDSP_SWAP_REG), TAS2559_PAGE_REG(TAS2559_mCDSP_SWAP_REG), 0x00,
		0x00, 0x00, 0x01, 0x00,
	};

	/* write the commit sequence */
	memcpy(&gpDevABlock[gnDevABlockIndex*4], pCommit, 12);
	gnDevABlockIndex += 3;

	/* write number of commands for calibration block */
	gpDevABlock[4] = ((gnDevABlockIndex-2) & 0xFF000000) >> 24;
	gpDevABlock[5] = ((gnDevABlockIndex-2) & 0x00FF0000) >> 16;
	gpDevABlock[6] = ((gnDevABlockIndex-2) & 0x0000FF00) >> 8;
	gpDevABlock[7] = ((gnDevABlockIndex-2) & 0x000000FF);

	memcpy(&gpBin[gnBinIndex], gpDevABlock, gnDevABlockIndex*4);
	gnBinIndex += (gnDevABlockIndex*4);

	/* write bin file size */
	gpBin[4] = (gnBinIndex & 0xFF000000) >> 24;
	gpBin[5] = (gnBinIndex & 0x00FF0000) >> 16;
	gpBin[6] = (gnBinIndex & 0x0000FF00) >> 8;
	gpBin[7] = (gnBinIndex & 0x000000FF);

	write(gBinFile, gpBin, gnBinIndex);
	close(gBinFile);

	gBinFile = 0;
}

// ----------------------------------------------------------------------------- 
// check_spk_bounds 
// ----------------------------------------------------------------------------- 
// Description: 
//      Checks if speaker paramters are within bounds. 
// ----------------------------------------------------------------------------- 
uint32_t check_spk_bounds(struct TFTCConfiguration *pFTCC, double re1, double re2) 
{ 
	uint32_t result = RESULT_PASS; 

	if(re1>pFTCC->nTSpkCharDevA.nReHi) 
		result |= RE1_FAIL_HI; 
	if(re1<pFTCC->nTSpkCharDevA.nReLo) 
		result |= RE1_FAIL_LO; 
   
    if(re2>pFTCC->nTSpkCharDevB.nReHi) 
        result |= RE2_FAIL_HI; 
    if(re2<pFTCC->nTSpkCharDevB.nReLo) 
        result |= RE2_FAIL_LO; 

	return result; 
} 

void tas2559_ftc_release(void)
{
	if (gTILoad > 0) {
		close(gTILoad);
		gTILoad = 0;
	}
}
