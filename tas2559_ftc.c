/*
** =============================================================================
** Copyright (c) 2016  Texas Instruments Inc.
**
** File:
**     tas2559_ftc.c
**
** Description:
**     factory test program for TAS2559 Android devices
**
** =============================================================================
*/

#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <sys/types.h>

#include "system.h"
#include "tas2559.h"	// TAS2559 Driver
#include "tas2559_ftc_lib.h"    
#include "tas2559_ftc.h"	// TAS2559 Factory Test and Calibration Tool

#define PI		3.14159

// -----------------------------------------------------------------------------
// tas2559_ftc
// -----------------------------------------------------------------------------
// Description:
//      Obtains Re, f0, Q and T_cal from the speaker. This only needs to be
//      executed once during production line test.
// -----------------------------------------------------------------------------
uint32_t tas2559_ftc(double t_cal, struct TFTCConfiguration *pFTCC)
{
	int nResult = 0;
	double dev_a_re = pFTCC->nTSpkCharDevA.nPPC3_Re0;   // Default Re
	uint32_t dev_a_prm_pow = 0;           // Total RMS power coefficient
	uint32_t dev_a_prm_tlimit = 0;        // Delta temperature limit coefficient
	double dev_b_re = pFTCC->nTSpkCharDevB.nPPC3_Re0;   // Default Re
	uint32_t dev_b_prm_pow = 0;           // Total RMS power coefficient
	uint32_t dev_b_prm_tlimit = 0;        // Delta temperature limit coefficient
	uint8_t nPGID;
	uint32_t libVersion;
	uint32_t result = 0;
	pid_t nPlaybackProcess;
	int i;

	libVersion = get_lib_ver();
	printf("libVersion=0x%x\r\n", libVersion);
	tas2559_switch_device(pFTCC->nTSpkCharDevA.nDevAddr);

	// STEP 1: Play calibration signal
	tas2559_mixer_command("PRI_MI2S_RX Audio Mixer MultiMedia1", 1); //platform dependent
	nPlaybackProcess = sys_play_wav("silense.wav", "loop");

	// STEP 2: start calibration process
	tas2559_ftc_start(DevBoth, pFTCC->nTSpkCharDevA.nPPC3_Re0);

	// STEP 3: Wait for algorithm to converge
	sys_delay(pFTCC->nCalibrationTime); // Delay 

	// STEP 4: Get actual Re from TAS2559
	dev_a_re = get_re(DevA, pFTCC->nTSpkCharDevA.nPPC3_Re0);
	dev_b_re = get_re(DevB, pFTCC->nTSpkCharDevB.nPPC3_Re0);
	printf("after %d ms, dev_a Re=%f, dev_b Re=%f\r\n", pFTCC->nCalibrationTime, dev_a_re, dev_b_re);

	// STEP 5: check speaker bounds
	nResult = check_spk_bounds(pFTCC, dev_a_re, dev_b_re);

	// STEP 6: Set temperature limit to target TMAX
	if ((nResult& RE1_CHK_MSK) == RESULT_PASS) {
		dev_a_prm_pow = calc_prm_pow (dev_a_re,
			pFTCC->nTSpkCharDevA.nSpkTMax - t_cal,
			pFTCC->nTSpkCharDevA.nPPC3_RTV,
			pFTCC->nTSpkCharDevA.nPPC3_RTM,
			pFTCC->nTSpkCharDevA.nPPC3_RTVA,
			pFTCC->nTSpkCharDevA.nPPC3_SysGain);
		dev_a_prm_tlimit = calc_prm_tlimit(pFTCC->nTSpkCharDevA.nSpkTMax - t_cal, 
			pFTCC->nTSpkCharDevA.nSpkReAlpha, 
			pFTCC->nTSpkCharDevA.nPPC3_DevNonlinPer, 
			pFTCC->nTSpkCharDevA.nPPC3_RTV, 
			pFTCC->nTSpkCharDevA.nPPC3_RTM, 
			pFTCC->nTSpkCharDevA.nPPC3_RTVA,
			pFTCC->nTSpkCharDevA.nPPC3_PIG);
		set_re(DevA, pFTCC->nTSpkCharDevA.nPPC3_Re0, dev_a_re, pFTCC->nTSpkCharDevA.nSpkReAlpha);
		set_temp_cal(DevA, dev_a_prm_pow, dev_a_prm_tlimit);
	}

 	if ((nResult& RE2_CHK_MSK) == RESULT_PASS) {
		dev_b_prm_pow = calc_prm_pow (dev_b_re, 
			pFTCC->nTSpkCharDevB.nSpkTMax - t_cal, 
			pFTCC->nTSpkCharDevB.nPPC3_RTV, 
			pFTCC->nTSpkCharDevB.nPPC3_RTM, 
			pFTCC->nTSpkCharDevB.nPPC3_RTVA, 
			pFTCC->nTSpkCharDevB.nPPC3_SysGain);
		dev_b_prm_tlimit = calc_prm_tlimit(pFTCC->nTSpkCharDevB.nSpkTMax - t_cal, 
			pFTCC->nTSpkCharDevB.nSpkReAlpha, 
			pFTCC->nTSpkCharDevB.nPPC3_DevNonlinPer, 
			pFTCC->nTSpkCharDevB.nPPC3_RTV, 
			pFTCC->nTSpkCharDevB.nPPC3_RTM, 
			pFTCC->nTSpkCharDevB.nPPC3_RTVA,
			pFTCC->nTSpkCharDevB.nPPC3_PIG);
		set_re(DevB, pFTCC->nTSpkCharDevB.nPPC3_Re0, dev_b_re, pFTCC->nTSpkCharDevB.nSpkReAlpha);
		set_temp_cal(DevB, dev_b_prm_pow, dev_b_prm_tlimit);
	}

	// STEP 7: stop calibration process
	tas2559_ftc_stop();

	sys_stop_wav(nPlaybackProcess);

	// STEP 8: Save Re, and Cal Temp into a file
	tas2559_save_cal(pFTCC, dev_a_re, dev_a_prm_pow, dev_a_prm_tlimit, 
		dev_b_re, dev_b_prm_pow, dev_b_prm_tlimit, 
		t_cal, nResult, "tas2559_cal.txt");

    // STEP 9: Save .bin file for TAS2555 driver
	if ((nResult & (RE1_CHK_MSK | RE2_CHK_MSK)) == RESULT_PASS) {
		tas2559_open_bin("tas2559_cal.bin");
		if ((nResult & RE1_CHK_MSK) == RESULT_PASS) {
			set_re(DevA, pFTCC->nTSpkCharDevA.nPPC3_Re0, dev_a_re, pFTCC->nTSpkCharDevA.nSpkReAlpha);
			set_temp_cal(DevA, dev_a_prm_pow, dev_a_prm_tlimit);
		}

		if ((nResult & RE2_CHK_MSK) == RESULT_PASS) {
			set_re(DevB, pFTCC->nTSpkCharDevB.nPPC3_Re0, dev_b_re, pFTCC->nTSpkCharDevB.nSpkReAlpha);
			set_temp_cal(DevB, dev_b_prm_pow, dev_b_prm_tlimit);
		}

		tas2559_close_bin();
	}

	if (pFTCC->bLoadCalibration)
		tas2559_load_calibration(0xFF);

	tas2559_ftc_release();

    return result;
}
