/*
** =============================================================================
** Copyright (c) 2016  Texas Instruments Inc.
**
** File:
**     system.c
**
** Description:
**     system functions for TAS2559 factory test program
**
** =============================================================================
*/

#include "system.h"

#include <sys/wait.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

pid_t sys_play_wav(char * pFile, char * pMode)
{
	pid_t nProcess;
	int nStatus, nTimeout;
	char *pArgs[] = {AUDIO_PLAYER, pFile, NULL};
	char *pEnv[] = {NULL};

	if (0 == (nProcess = fork()))
	{
		if (execve(AUDIO_PLAYER, pArgs, pEnv) == -1)
		{
			printf("factorytest: Can't play %s. Please install %s. \n\r", pFile, AUDIO_PLAYER);
			exit(-1);
		}
	}

	sys_delay(500);

	printf("factorytest: Started playback of %s\n\r", pFile);
	
	return nProcess;  
}

extern void sys_stop_wav(pid_t nProcess)
{
	char *pEnv[] = {NULL};

	printf("factorytest: Stop playback.\n\r");

	kill(nProcess, SIGKILL);
}

extern void sys_delay(uint32_t delay_ms)
{
	usleep(delay_ms * 1000);
}

extern double sys_get_ambient_temp(void)
{
	return 0.0;
}

extern int sys_is_valid(char * pFile)
{
	return 0;
}

