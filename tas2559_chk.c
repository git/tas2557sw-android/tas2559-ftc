/*
** =============================================================================
** Copyright (c) 2016  Texas Instruments Inc.
**
** File:
**     tas2559_ftc.c
**
** Description:
**     factory test program for TAS2559 Android devices
**
** =============================================================================
*/

#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <sys/types.h>

#include "system.h"
#include "tas2559.h"        // TAS2559 Driver
#include "tas2559_ftc_lib.h"    
#include "tas2559_ftc.h"    // TAS2559 Factory Test and Calibration Tool

#define PI                   3.14159

// -----------------------------------------------------------------------------
// tas2559_ftc
// -----------------------------------------------------------------------------
// Description:
//      Obtains Re, f0, Q and T_cal from the speaker. This only needs to be
//      executed once during production line test.
// -----------------------------------------------------------------------------
int tas2559_chk(double t_cal, struct TFTCConfiguration *pFTCC)
{
	int nResult = 0;
	double nDevARe, nDevADeltaT, nDevAF0, nDevAQ;
	double nDevBRe, nDevBDeltaT, nDevBF0, nDevBQ;
	uint8_t nPGID;
	uint32_t libVersion;

	libVersion = get_lib_ver();
	printf("libVersion=0x%x\r\n", libVersion);

	/* Get actual Re from TAS2559 */
	nResult = get_Re_deltaT(DevA,
							pFTCC->nTSpkCharDevA.nPPC3_Re0,
							pFTCC->nTSpkCharDevA.nSpkReAlpha,
							&nDevARe, &nDevADeltaT);
	nResult = get_f0_Q(DevA,
						pFTCC->nTSpkCharDevA.nPPC3_FWarp,
						pFTCC->nPPC3_FS,
						pFTCC->nTSpkCharDevA.nPPC3_Bl,
						pFTCC->nTSpkCharDevA.nPPC3_Mms,
						pFTCC->nTSpkCharDevA.nPPC3_Re0,
						&nDevAF0, &nDevAQ);
	printf("SPK_A Re = %f, DeltaT=%f, F0 = %f, Q = %f\n", nDevARe, nDevADeltaT, nDevAF0, nDevAQ);

	nResult = get_Re_deltaT(DevB,
							pFTCC->nTSpkCharDevB.nPPC3_Re0,
							pFTCC->nTSpkCharDevB.nSpkReAlpha,
							&nDevBRe, &nDevBDeltaT);
	nResult = get_f0_Q(DevB,
						pFTCC->nTSpkCharDevB.nPPC3_FWarp,
						pFTCC->nPPC3_FS,
						pFTCC->nTSpkCharDevB.nPPC3_Bl,
						pFTCC->nTSpkCharDevB.nPPC3_Mms,
						pFTCC->nTSpkCharDevB.nPPC3_Re0,
						&nDevBF0, &nDevBQ);
	printf("SPK_B Re = %f, DeltaT=%f, F0 = %f, Q = %f\n", nDevBRe, nDevBDeltaT, nDevBF0, nDevBQ);

	tas2559_ftc_release();

    return nResult;
}
